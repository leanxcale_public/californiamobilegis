package com.leanxcale.service.creator.impl;

import java.io.IOException;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Database;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.service.creator.MetadataCreator;

public abstract class AbstractMetadataCreatorImpl implements MetadataCreator {
	
	private String table;
	private String sequence;
	
	public String getTable() {
		return table;
	}
	protected void setTable(String table) {
		this.table = table;
	}
	public String getSequence() {
		return sequence;
	}
	protected void setSequence(String sequence) {
		this.sequence = sequence;
	}
	
	@Override
	public void cleanDemo(String lxUrl, String database, String user, String password) throws IOException, LeanxcaleException {
		
		setTable();
		setSequence();
		
		Settings settings = new Settings();
		Credentials credentials = new Credentials();
		credentials.setDatabase(database).setUser(user);
		credentials.setDatabase(database).setPass(password.toCharArray());
		settings.credentials(credentials);

		Session session = null;
		try {
			session = SessionFactory.newSession(lxUrl,settings);
			Database databaseInstance = session.database();
			if (databaseInstance.tableExists(table)) {
				session.database().dropTable(table);
			}
			createTable(session);
			if (sequence!=null && !sequence.isEmpty()) {
				try {
					session.database().createSequence(sequence, 1);
				} catch (Exception e) {
					//Empty catch caused by temporary bug
				}
			}
		}	
		finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public abstract void setTable();
	
	public abstract void setSequence();
	
	public abstract void createTable(Session session);

}
