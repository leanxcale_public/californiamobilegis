package com.leanxcale.service.creator.impl;

import java.util.Arrays;
import java.util.Collections;

import com.leanxcale.kivi.database.Database;
import com.leanxcale.kivi.database.Field;
import com.leanxcale.kivi.database.Type;
import com.leanxcale.kivi.session.Session;


public class CaliforniaMobileMetadataCreatorImpl extends AbstractMetadataCreatorImpl{
	
	private static final String table = "GPS";
	private static final String sequence = "GPS_ID_SEQ";
	
	@Override
	public void createTable(Session session) {
		// Gets the database object (to access the schema)
		Database database = session.database();

		// Checks the existence of the table
		if (!database.tableExists(getTable())) {

			// If the table does not exist creates it.
			database.createTable(getTable(), 
					Collections.singletonList(new Field("ID", Type.LONG)), // PK
					Arrays.asList(new Field("VEHICLE", Type.STRING), new Field("MOMENT", Type.TIMESTAMP), 
							new Field("LOCATION", Type.STRING),new Field("SPEED", Type.DOUBLE)));
		}
	}

	@Override
	public void setTable() {
		setTable(table);
	}

	@Override
	public void setSequence() {
		setSequence(sequence);
	}

}
