package com.leanxcale.service;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import com.leanxcale.exception.LeanxcaleException;

public interface GISDemoFunctionalService {
	
    public void runDemo(String lxUrl, String name, String database, String usr, String passwd, String dataset, AtomicReference status) throws IOException;
	
	public void cleanDemo(String lxUrl, String name, String database, String usr, String passwd) throws IOException, InstantiationException, LeanxcaleException, IllegalAccessException, ClassNotFoundException;

}
