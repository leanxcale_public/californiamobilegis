package com.leanxcale.service.factory.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.leanxcale.service.factory.DataLoaderFactory;
import com.leanxcale.service.loader.DataLoader;

@Service
public class DataLoaderFactoryImpl implements DataLoaderFactory {
	
	private static final String propsName = "factory/dataLoaderFactory.properties";
	
	private static Map<String, String> classes = null;
	
	@Override
	public DataLoader getLoader(String name) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (classes == null) loadDataLoaderFactoryProperties();
		return (DataLoader)Class.forName(classes.get(name)).newInstance();
	}
	
	private void loadDataLoaderFactoryProperties () throws IOException {
		
		Properties prop = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsName);
		prop.load(in);
		
		classes = new HashMap<String, String>();
		Enumeration<String> enums = (Enumeration<String>) prop.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			classes.put(key, prop.getProperty(key));
		}
	}

}
