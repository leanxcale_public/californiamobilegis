package com.leanxcale.service.factory;

import java.io.IOException;

import com.leanxcale.service.creator.MetadataCreator;

public interface MetadataCreatorFactory {
	
	public MetadataCreator getCreator(String name) throws InstantiationException, IOException, IllegalAccessException, ClassNotFoundException;

}
