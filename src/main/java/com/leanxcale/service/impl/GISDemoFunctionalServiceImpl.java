package com.leanxcale.service.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.service.GISDemoFunctionalService;
import com.leanxcale.service.factory.DataLoaderFactory;
import com.leanxcale.service.factory.MetadataCreatorFactory;

@Service
public class GISDemoFunctionalServiceImpl implements GISDemoFunctionalService {

	@Autowired
	private DataLoaderFactory dataLoaderFactory;

	@Autowired
	private MetadataCreatorFactory metadataCreatorFactory;

	private List<Thread> pool = new ArrayList<Thread>();
		

	public List<Thread> getPool() {
		return pool;
	}

	public void setPool(List<Thread> pool) {
		this.pool = pool;
	}

	@Override
	public void runDemo(String lxUrl, String name, String database, String user, String password, String dataset, AtomicReference status) throws IOException {

			try {
				Map<String, List<String>> rootProperties = loadCsvNames(name, dataset);
	
				// Get the appropriate csv list
				List<String> csvs = rootProperties.get(name);
				for (String csvName: csvs) {
					Thread t = new Thread(() -> {
						try {
							dataLoaderFactory.getLoader(name).loadData(lxUrl, csvName, status, database, user, password);
						} catch (IOException | LeanxcaleException | InterruptedException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
							e.printStackTrace();
						}
					});
					getPool().add(t);
					t.start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void cleanDemo(String lxUrl, String name, String database, String user, String password) throws IOException, InstantiationException, LeanxcaleException, IllegalAccessException, ClassNotFoundException {
		metadataCreatorFactory.getCreator(name).cleanDemo(lxUrl, database, user, password);
	}

	private Map<String, List<String>> loadCsvNames(String name, String dataset) throws IOException {
		// Load properties file to get files to load
		Properties props = new Properties();
		// Try to find external file
		String currentDir = System.getProperty("user.dir");
		InputStream ins;
		try {
			ins = new FileInputStream(currentDir + "/" + dataset);
		}
		catch (FileNotFoundException e) {
			// External properties not found. Get the internal one
			ins = Thread.currentThread().getContextClassLoader().getResourceAsStream(dataset);
		}
		props.load(ins);
		
		Map<String, List<String>> res = new HashMap<String, List<String>>();
		
		String csvsString = (String) props.getProperty("csvs");
		String[] parts = csvsString.split(",");
		if(res.get(name) == null) {
			List<String> csvs = new ArrayList<String>();
			for (int i = 0; i < parts.length; i++) {
				csvs.add(parts[i]);
			}
			res.put(name, csvs);
		}
		return res;
	}
}
