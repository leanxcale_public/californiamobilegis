package com.leanxcale.service.loader.impl;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.WKTWriter;

import com.leanxcale.kivi.database.Sequence;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.tuple.Tuple;



public class CaliforniaMobileCSVLoaderImpl extends AbstractCSVDataLoaderImpl {
	
	private static final String table = "GPS";
	private static final String sequence = "GPS_ID_SEQ";

	@Override
	public Tuple row2Tuple(String path, String[] rowParts, Session session) {
		// CSV FORMAT: unixtime(long);latitude(double);longitude(double);speed(double)
		// Position in WKT
		GeometryFactory geometryFactory = new GeometryFactory();
	    Coordinate coords = new Coordinate(Double.parseDouble(rowParts[1]),Double.parseDouble(rowParts[2]));
	    Geometry point = geometryFactory.createPoint(coords);
		WKTWriter writer = new WKTWriter();
		String position = writer.write(point);
		// Get next sequence value
		Sequence idSeq = session.database().getSequence(getSequence());
		long nextSeqVal = idSeq.nextVal();
		// Create timestamp
		Timestamp t = new Timestamp(Long.parseLong(rowParts[0]));
		// Get vehicle id
		Pattern pattern = Pattern.compile(".*/(\\w+).\\w+");
		Matcher matcher = pattern.matcher(path);
		String vehicleId = null;
		if (matcher.matches()) {
			vehicleId = matcher.group(1);
		}		
		// Generate tuple to insert
		Table table = session.database().getTable(getTable());
		Tuple tuple = generateTuple(table, nextSeqVal, vehicleId, t, position,
				Double.parseDouble(rowParts[3]));
		// Insert tuple
		System.out.println("Thread inserting record from csv " + path + " with id " + nextSeqVal);
		return tuple;
	}
	
	private static Tuple generateTuple(Table table, long id, String vehicle, Timestamp timestamp, String wkt,
			Double speed) {

		Tuple tuple = table.createTuple();
		tuple.putLong("ID", id);
		tuple.putString("VEHICLE", vehicle);
		tuple.putTimestamp("MOMENT", timestamp);
		tuple.putString("LOCATION", wkt);
		tuple.putDouble("SPEED", speed);

		return tuple;
	}

	@Override
	public void setTable() {
		setTable(table);
	}

	@Override
	public void setPeriod() {
		setPeriod(1000);
	}

	@Override
	public void setSequence() {
		setSequence(sequence); 
		
	}

	

}
