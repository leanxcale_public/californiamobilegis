package com.leanxcale.service.loader.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicReference;

import com.leanxcale.constants.GISDemoConstants;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import com.leanxcale.service.loader.DataLoader;

public abstract class AbstractCSVDataLoaderImpl implements DataLoader {
	
	private String table;
	private String sequence;
	private long period;
	
	protected String getTable() {
		return this.table;
	}
	
	protected void setTable(String table) {
		this.table = table;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	protected long getPeriod() {
		return this.period;
	}
	
	protected void setPeriod(long period) {
		this.period = period;
	}
	
	@Override
	public void loadData(String lxUrl, String path, AtomicReference<String> status, String database, String user, String password) throws IOException, LeanxcaleException, InterruptedException {
		
		setTable();
		setSequence();
		setPeriod();
		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		Settings settings = new Settings();
		Credentials credentials = new Credentials();
		credentials.setDatabase(database).setUser(user);
		credentials.setDatabase(database).setPass(password.toCharArray());
		settings.credentials(credentials);
		
		Session session = null;
		try { 
			session = SessionFactory.newSession(lxUrl,settings);
			// Get table
			Table table = session.database().getTable(getTable());
			// Get csv file
			InputStream in;
			in = new FileInputStream(path);
	        br = new BufferedReader(new InputStreamReader(in));
	        int lineNumber = 1;
	        boolean stillLines = true;
			while ( status.get().equals(GISDemoConstants.STATUS_RUNNING)) {
				line = br.readLine();
				if (line != null) {
					if (lineNumber != 1) {
						// Use comma as separator
						String[] parts = line.split(cvsSplitBy);
						Tuple tuple = row2Tuple(path, parts, session);
						table.upsert(tuple);
						session.commit();
						Thread.sleep(getPeriod());
					}
					lineNumber++;
				}
				else {
					in = getClass().getClassLoader().getResourceAsStream(path);	
					br = new BufferedReader(new InputStreamReader(in));
					lineNumber=1;
				}
			}
		} finally {
			if (br != null) {
				br.close();
			}
			if (session != null) {
				session.close();
			}
		}
	}
	
	public abstract Tuple row2Tuple (String path, String[] rowParts, Session session);
	
	public abstract void setTable();
	
	public abstract void setSequence();
	
	public abstract void setPeriod();

}
