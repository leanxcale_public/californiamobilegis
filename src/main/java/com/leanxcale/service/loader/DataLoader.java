package com.leanxcale.service.loader;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import com.leanxcale.exception.LeanxcaleException;

public interface DataLoader {
	
	public void loadData (String lxUrl ,String path, AtomicReference<String> status, String database, String user, String password) throws IOException, LeanxcaleException, InterruptedException;

}
