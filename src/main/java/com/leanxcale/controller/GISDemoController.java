
package com.leanxcale.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.leanxcale.exception.LeanxcaleException;

@RequestMapping("/default")
public interface GISDemoController {
		
	@GetMapping("/run")
    public void runDemo() throws IOException;
	
	@GetMapping("/stop")
	public void stopDemo() throws IOException;
	
	@GetMapping("/clean")
	public void cleanDemo() throws IOException, InstantiationException, LeanxcaleException, IllegalAccessException, ClassNotFoundException;
	
	@GetMapping("/status")
	public String getStatus();
}
