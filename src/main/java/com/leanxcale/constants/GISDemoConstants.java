package com.leanxcale.constants;

public interface GISDemoConstants {
	
	public static final String STATUS_RUNNING = "running";
	public static final String STATUS_STOPPED = "stopped";
	public static final String STATUS_CLEAN = "clean";

}
