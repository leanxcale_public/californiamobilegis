# Mobile Century GIS Demo over LeanXcale

Download code and execute:

```
mvn spring-boot:run
```

To package a jar, run:

```
mvn clean package
```

And then run created jar:

```
java -jar GISDemo-0.0.1-SNAPSHOT.jar
```

In both cases, a Spring Boot Tomcat server is started on por 8080.

# Run demo

A LeanXcale database is needed to support this demo. Once the Tomcat server is started, it listens to 3 kinds of requests:

1. Run

    Automatically starts data insertion into LX. Data is parsed from csv files contained in sources/csv.
    Request format example: http://localhost:8080/GISDemo/run

2. Stop

    Stop insertion.
    Request format example: http://localhost:8080/GISDemo/stop

3. Clear

    Clean database table and re-creates schema.
    Request format example: http://localhost:8080/GISDemo/clean

# Class diagram

Please find class diagram on wiki page https://gitlab.com/leanxcale_public/californiamobilegis/wikis/Class-Diagram


